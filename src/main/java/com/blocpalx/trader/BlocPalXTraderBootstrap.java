package com.blocpalx.trader;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.BlocPalXApiWebSocketClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@Configuration
@EnableAsync
@EnableScheduling
@Slf4j
@PropertySource("classpath:trader.properties")
public class BlocPalXTraderBootstrap {

    @Value("${blocpalx.env:#{null}}")
    private String env;

    @Value("${blocpalx.apiKey:#{null}}")
    private String apiKey;

    @Value("${blocpalx.apiSecret:#{null}}")
    private String apiSecret;

    public static void main(String[] args) {
        SpringApplication.run(BlocPalXTraderBootstrap.class, args);
    }

    @Bean
    public BlocPalXApiRestClient blocPalXApiRestClient() {
        log.info("API KEY: {}", apiKey);
        BlocPalXApiRestClient restClient = BlocPalXApiClientFactory.newInstance(getEnv(), apiKey, apiSecret).newRestClient();
        log.info("REST URL: " + restClient.getBaseUrl());
        return restClient;
    }

    @Bean
    public BlocPalXApiWebSocketClient blocPalXApiWebSocketClient() {
        BlocPalXApiWebSocketClient wsClient = BlocPalXApiClientFactory.newInstance(getEnv(), apiKey, apiSecret).newWebSocketClient();
        log.info("WSS URL: " + wsClient.getBaseUrl());
        return wsClient;
    }

    private BlocPalXEnvironment getEnv() {
        switch (env) {
            case "local":
                return BlocPalXEnvironment.LOCAL;
            case "sandbox":
                return BlocPalXEnvironment.SANDBOX;
            case "production":
                return BlocPalXEnvironment.PRODUCTION;
        }
        throw new IllegalArgumentException("Invalid blocpalx.env");
    }
}
