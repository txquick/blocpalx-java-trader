package com.blocpalx.trader.context;

import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.domain.OrderSide;
import com.blocpalx.api.client.domain.account.NewOrderResponse;
import com.blocpalx.api.client.domain.account.OrderFill;
import com.blocpalx.api.client.domain.account.Trade;
import com.blocpalx.api.client.domain.market.OrderBook;
import com.blocpalx.api.client.domain.market.request.OrderBookDepthLimit;
import com.blocpalx.trader.domain.MarketTradePair;
import com.blocpalx.trader.domain.TradeStrategy;
import com.blocpalx.trader.service.MarketDataService;
import com.blocpalx.trader.service.UserDataService;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;

/**
 * For posterity's sake - this is intended to
 *   (a) populate candlesticks through the alpha/beta stages until we have natural traffic
 *   (b) serve as a template/example of how to setup a trade strategy within this application
 */
@Slf4j
public class TradingVolumeContext extends TradingContext {

    private final static int RANDOM_MIN = 1;
    private final static int RANDOM_MAX = 30;

    private final Random random = new Random();

    private NewOrderResponse lastOrder;

    public TradingVolumeContext(BlocPalXApiRestClient restClient,
                                MarketDataService marketDataService,
                                UserDataService userDataService,
                                TradeStrategy tradeStrategy,
                                MarketTradePair tradePair) {
        super(restClient, marketDataService, userDataService, tradeStrategy, tradePair);
        log.info("Starting Trade Pair: {}-{}", tradePair.getBaseAsset(), tradePair.getQuoteAsset());
        initialize();
    }

    private void initialize() {
        checkForTrades();
    }

    @Override
    public void checkForTrades() {
        if (userDataService.assetIsTradable(tradePair.getBaseAsset())) {
            OrderBook orderBook = restClient.getOrderBook(tradePair.getSymbol(), OrderBookDepthLimit.FIVE);
            if (orderBook != null) {
                BigDecimal currentPrice = getCurrentPrice(orderBook);

                BigDecimal myLastTradePrice = BigDecimal.ZERO;
                if (lastOrder != null) {
                    myLastTradePrice = new BigDecimal(lastOrder.getPrice());
                } else {
                    List<Trade> myTrades = restClient.getMyTrades(tradePair.getSymbol());
                    // log.info("{} myTrades: {}", tradePair.getSymbol(), myTrades);
                    if (myTrades.size() > 0) {
                        myLastTradePrice = new BigDecimal(myTrades.get(0).getPrice());
                    }
                }

                if (currentPrice == null || currentPrice.equals(BigDecimal.ZERO)) {
                    // no current price - do nothing...
                    log.info("{} currentPrice is unavailable", tradePair.getSymbol());
                } else if (myLastTradePrice.equals(BigDecimal.ZERO)) {
                    log.info("{} currentPrice: {}, myLastTradePrice: {}", tradePair.getSymbol(),
                            currentPrice.toPlainString(), myLastTradePrice.toPlainString());
                    calculateTrade(true, null);
                } else {
                    BigDecimal percentChange = BigDecimal.ZERO;
                    if (myLastTradePrice.compareTo(BigDecimal.ZERO) > 0 && currentPrice.compareTo(BigDecimal.ZERO) > 0) {
                        percentChange = percentChange(myLastTradePrice, currentPrice).abs();
                    }

                    // doing this for now just so I can see some candlesticks on the USDT-USDC pair
                    boolean randomTrade = (random.nextInt((RANDOM_MAX - RANDOM_MIN) + 1) + RANDOM_MIN) == 1;

                    log.info("{} currentPrice: {}, myLastTradePrice: {}, percentChange: {}, randomTrade: {}",
                            tradePair.getSymbol(),
                            currentPrice.toPlainString(),
                            myLastTradePrice.toPlainString(),
                            percentChange.toPlainString(),
                            (randomTrade ? "true" : "false"));

                    if (randomTrade || percentChange.compareTo(BigDecimal.ONE) > 0) {
                        calculateTrade(true, null);
                    }
                }
            } else {
                log.error("Empty order book: {}", tradePair.getSymbol());
            }
        } else {
            log.error("Account cannot trade: {}", tradePair.getBaseAsset());
        }
    }

    private void calculateTrade(boolean cascade, OrderSide orderSideOverride) {
        OrderSide orderSide = orderSideOverride;
        if (orderSide == null) {
            if (lastOrder == null || lastOrder.getSide() == OrderSide.BUY) {
                orderSide = OrderSide.SELL;
            } else {
                orderSide = OrderSide.BUY;
            }
        }

        if (orderSide == OrderSide.SELL) {
            BigDecimal dollarsWorth = new BigDecimal("1.5").multiply(BigDecimal.ONE.divide(marketDataService.getMarketAssetValues().get(tradePair.getBaseAsset()).getValueUsd(), 8, RoundingMode.FLOOR));

            if (checkSufficientBalance(tradePair.getBaseAsset(), dollarsWorth)) {
                log.info("{} EXECUTING SELL {} {}", tradePair.getSymbol(), dollarsWorth, tradePair.getBaseAsset());
                lastOrder = marketSell(tradePair.getSymbol(), dollarsWorth.toPlainString(), "");
                lastOrder.setPrice(calculateWeightedTradePrice(lastOrder).toPlainString());
                log.info("{}", lastOrder);
            } else {
                log.info("{} {} balance is insufficient to place a {} SELL", tradePair.getSymbol(), tradePair.getBaseAsset(), dollarsWorth);
                if (cascade) {
                    // try the other direction
                    calculateTrade(false, OrderSide.BUY);
                }
            }
        }

        if (orderSide == OrderSide.BUY) {
            BigDecimal dollarsWorth = new BigDecimal("1.5").multiply(BigDecimal.ONE.divide(marketDataService.getMarketAssetValues().get(tradePair.getQuoteAsset()).getValueUsd(), 8, RoundingMode.FLOOR));

            if (checkSufficientBalance(tradePair.getQuoteAsset(), dollarsWorth)) {
                log.info("{} EXECUTING BUY {} using {} {}", tradePair.getSymbol(), tradePair.getBaseAsset(), dollarsWorth, tradePair.getQuoteAsset());
                lastOrder = marketBuy(tradePair.getSymbol(), "", dollarsWorth.toPlainString());
                lastOrder.setPrice(calculateWeightedTradePrice(lastOrder).toPlainString());
                log.info("{}", lastOrder);
            } else {
                log.info("{} {} balance is insufficient to place a {} BUY", tradePair.getSymbol(), tradePair.getQuoteAsset(), dollarsWorth);
                if (cascade) {
                    // try the other direction
                    calculateTrade(false, OrderSide.SELL);
                }
            }
        }
    }

}
