package com.blocpalx.trader.context;

import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.domain.account.*;
import com.blocpalx.api.client.domain.market.OrderBook;
import com.blocpalx.trader.domain.MarketTradePair;
import com.blocpalx.trader.domain.TradeStrategy;
import com.blocpalx.trader.service.MarketDataService;
import com.blocpalx.trader.service.UserDataService;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
public class TradingContext {

    private static final BigDecimal HUNDRED = new BigDecimal("100");
    private static final int SCALE = 8;

    protected final BlocPalXApiRestClient restClient;
    protected final MarketDataService marketDataService;
    protected final UserDataService userDataService;
    protected final TradeStrategy tradeStrategy;
    protected final MarketTradePair tradePair;

    public TradingContext(BlocPalXApiRestClient restClient,
                          MarketDataService marketDataService,
                          UserDataService userDataService,
                          TradeStrategy tradeStrategy,
                          MarketTradePair tradePair) {
        this.restClient = restClient;
        this.marketDataService = marketDataService;
        this.userDataService = userDataService;
        this.tradeStrategy = tradeStrategy;
        this.tradePair = tradePair;
    }

    public void checkForTrades() {
        // override this...
    }

    protected NewOrderResponse newOrder(NewOrder newOrder) {
        return restClient.newOrder(newOrder.newOrderRespType(NewOrderResponseType.FULL));
    }

    protected NewOrderResponse marketBuy(String symbol, String baseQuantity, String quoteQuantity) {
        NewOrder newOrder = NewOrder.marketBuy(symbol, baseQuantity, quoteQuantity).newOrderRespType(NewOrderResponseType.FULL);
        return restClient.newOrder(newOrder);
    }

    protected NewOrderResponse marketSell(String symbol, String baseQuantity, String quoteQuantity) {
        NewOrder newOrder = NewOrder.marketSell(symbol, baseQuantity, quoteQuantity).newOrderRespType(NewOrderResponseType.FULL);
        return restClient.newOrder(newOrder);
    }


    protected BigDecimal getCurrentPrice(OrderBook orderBook) {
        BigDecimal currentPrice = null;
        if (orderBook.getBids() != null && orderBook.getBids().size() > 0
                && orderBook.getAsks() != null && orderBook.getAsks().size() > 0) {
            currentPrice = (new BigDecimal(orderBook.getBids().get(0).getPrice()).add(new BigDecimal(orderBook.getAsks().get(0).getPrice()))).divide(new BigDecimal("2"), RoundingMode.HALF_EVEN);
        } else if (orderBook.getBids() != null && orderBook.getBids().size() > 0) {
            currentPrice = new BigDecimal(orderBook.getBids().get(0).getPrice());
        } else if (orderBook.getAsks() != null && orderBook.getAsks().size() > 0) {
            currentPrice = new BigDecimal(orderBook.getAsks().get(0).getPrice());
        }
        return currentPrice;
    }

    public BigDecimal percentChange(BigDecimal oldValue, BigDecimal newValue) {
        return divide(newValue.subtract(oldValue), oldValue).multiply(HUNDRED);
    }

    public BigDecimal divide(BigDecimal dividend, BigDecimal divisor) {
        return dividend.divide(divisor, SCALE, RoundingMode.HALF_EVEN);
    }

    public boolean checkSufficientBalance(String asset, BigDecimal quantity) {
        BigDecimal balance = BigDecimal.ZERO;
        AssetBalance assetBalance = userDataService.getBalances().get(asset);
        if (assetBalance != null) {
            balance = new BigDecimal(assetBalance.getFree());
        }

        return balance.compareTo(quantity) > 0;
    }

    public BigDecimal calculateWeightedTradePrice(NewOrderResponse orderResponse) {
        List<OrderFill> fills = orderResponse.getFills();
        RoundingMode roundingMode = RoundingMode.HALF_DOWN;

        BigDecimal num = BigDecimal.ZERO;
        BigDecimal denom = BigDecimal.ZERO;

        for (OrderFill fill : fills) {
            num = num.add(new BigDecimal(fill.getPrice()).multiply(new BigDecimal(fill.getQty())));
            denom = denom.add(new BigDecimal(fill.getQty()));
        }

        if (denom.compareTo(BigDecimal.ZERO) > 0) {
            return num.divide(denom, 8, roundingMode);
        } else {
            return BigDecimal.ZERO;
        }
    }
}
