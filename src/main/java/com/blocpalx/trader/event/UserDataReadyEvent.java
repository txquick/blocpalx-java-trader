package com.blocpalx.trader.event;

import org.springframework.context.ApplicationEvent;

public class UserDataReadyEvent extends ApplicationEvent {

    public UserDataReadyEvent(Object source) {
        super(source);
    }

}
