package com.blocpalx.trader.event;

import org.springframework.context.ApplicationEvent;

public class MarketDataReadyEvent extends ApplicationEvent {

    public MarketDataReadyEvent(Object source) {
        super(source);
    }

}
