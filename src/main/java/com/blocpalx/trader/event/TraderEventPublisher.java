package com.blocpalx.trader.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class TraderEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public TraderEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publishMarketDataReadyEvent() {
        applicationEventPublisher.publishEvent(new MarketDataReadyEvent(this));
    }

    public void publishUserDataReadyEvent() {
        applicationEventPublisher.publishEvent(new UserDataReadyEvent(this));
    }

}
