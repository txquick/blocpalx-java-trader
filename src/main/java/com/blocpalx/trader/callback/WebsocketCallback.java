package com.blocpalx.trader.callback;

import com.blocpalx.api.client.BlocPalXApiCallback;
import com.blocpalx.trader.service.WebsocketService;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Slf4j
public class WebsocketCallback<T> implements BlocPalXApiCallback<T> {

    private final AtomicReference<Consumer<T>> handler = new AtomicReference<>();
    private final WebsocketService websocketService;

    public WebsocketCallback (WebsocketService websocketService) {
        this.websocketService = websocketService;
    }

    @Override
    public void onResponse(final T response) {
        try {
            handler.get().accept(response);
        } catch (final Exception e) {
            log.info("Exception caught processing event: ", e);
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void onFailure(Throwable cause) {
        log.info("WS connection failed. Reconnecting.");
        websocketService.onFailure(cause);
    }

    public void setHandler(final Consumer<T> handler) {
        this.handler.set(handler);
    }
}