package com.blocpalx.trader.service;

import com.blocpalx.api.client.domain.general.SymbolStatus;
import com.blocpalx.trader.context.TradingContext;
import com.blocpalx.trader.context.TradingVolumeContext;
import com.blocpalx.trader.domain.MarketTradePair;
import com.blocpalx.trader.event.UserDataReadyEvent;
import com.blocpalx.trader.factory.TradingContextFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@EnableAsync
public class TradingContextsService {

    private final TradingContextFactory tradingContextFactory;
    private final MarketDataService marketDataService;

    private final List<TradingVolumeContext> tradingVolumeContexts = new ArrayList<>();

    @Autowired
    public TradingContextsService(TradingContextFactory tradingContextFactory,
                                  MarketDataService marketDataService) {
        this.tradingContextFactory = tradingContextFactory;
        this.marketDataService = marketDataService;
    }

    @EventListener(UserDataReadyEvent.class)
    public void initialize() {
        for (MarketTradePair tradingPair : marketDataService.getMarketTradePairs()) {
            if (tradingPair.getStatus() == SymbolStatus.TRADING) {
                tradingVolumeContexts.add(tradingContextFactory.createNewVolumeTrader(tradingPair));
            }
        }
    }

    @Async
    @Scheduled(initialDelayString = "60000", fixedRateString = "60000")
    public void runChecks() {
        for (TradingContext tradingContext : tradingVolumeContexts) {
            tradingContext.checkForTrades();
        }
    }

}
