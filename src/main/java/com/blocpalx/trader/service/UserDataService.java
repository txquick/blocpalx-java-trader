package com.blocpalx.trader.service;

import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.BlocPalXApiWebSocketClient;
import com.blocpalx.api.client.domain.account.Account;
import com.blocpalx.api.client.domain.account.AssetBalance;
import com.blocpalx.api.client.domain.event.AccountUpdateEvent;
import com.blocpalx.api.client.domain.event.OrderTradeUpdateEvent;
import com.blocpalx.api.client.domain.event.UserDataUpdateEvent;
import com.blocpalx.api.client.domain.general.SymbolStatus;
import com.blocpalx.api.client.domain.general.UserAsset;
import com.blocpalx.trader.ShutdownManager;
import com.blocpalx.trader.callback.WebsocketCallback;
import com.blocpalx.trader.domain.MarketTradePair;
import com.blocpalx.trader.event.MarketDataReadyEvent;
import com.blocpalx.trader.event.TraderEventPublisher;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

@Service
@Data
@Slf4j
public class UserDataService implements WebsocketService {

    private final BlocPalXApiRestClient restClient;
    private final BlocPalXApiWebSocketClient wsClient;
    private final TraderEventPublisher traderEventPublisher;
    private final ShutdownManager shutdownManager;
    private final WebsocketCallback<UserDataUpdateEvent> wsCallback;

    private volatile Closeable webSocket;

    private String listenKey;
    private Account account;
    private List<UserAsset> userAssets;
    private ConcurrentHashMap<String, AssetBalance> balances = new ConcurrentHashMap<>();

    @Autowired
    public UserDataService(BlocPalXApiRestClient restClient,
                           BlocPalXApiWebSocketClient wsClient,
                           TraderEventPublisher traderEventPublisher,
                           ShutdownManager shutdownManager) {
        this.restClient = restClient;
        this.wsClient = wsClient;
        this.traderEventPublisher = traderEventPublisher;
        this.shutdownManager = shutdownManager;
        this.wsCallback = new WebsocketCallback<>(this);
    }

    @EventListener(MarketDataReadyEvent.class)
    public void initialize() {
        connectUserDataStream();
        traderEventPublisher.publishUserDataReadyEvent();
    }

    public void connectUserDataStream() {
        // get the current balances before we start the stream
        loadUserAccount();

        // obtain a listenKey which is required to interact with the user data stream
        listenKey = restClient.startUserDataStream();
        log.info("ListenKey is: {}", listenKey);

        if (listenKey != null && !listenKey.isBlank()) {
            this.webSocket = wsClient.onUserDataUpdateEvent(listenKey, wsCallback);

            final Consumer<UserDataUpdateEvent> userDataUpdates = newEvent -> {
                if (newEvent.getEventType() == UserDataUpdateEvent.UserDataUpdateEventType.ACCOUNT_UPDATE) {
                    AccountUpdateEvent accountUpdateEvent = newEvent.getAccountUpdateEvent();
                    // Print new balances of every available asset
                    log.info("AccountUpdate Balance: {}", accountUpdateEvent.getBalances());
                    for (AssetBalance assetBalance : accountUpdateEvent.getBalances()) {
                        balances.put(assetBalance.getAsset(), assetBalance);
                    }

                } else if (newEvent.getEventType() == UserDataUpdateEvent.UserDataUpdateEventType.ACCOUNT_POSITION_UPDATE) {
                    AccountUpdateEvent accountUpdateEvent = newEvent.getAccountUpdateEvent();
                    // Print new balance of updated asset
                    log.info("AccountPositionUpdate Balance: {}", accountUpdateEvent.getBalances());
                    for (AssetBalance assetBalance : accountUpdateEvent.getBalances()) {
                        balances.put(assetBalance.getAsset(), assetBalance);
                    }
                } else {
                    OrderTradeUpdateEvent orderTradeUpdateEvent = newEvent.getOrderTradeUpdateEvent();

                    // Print details about the order/trade
                    log.info("{}: Order Status {} Price {}", orderTradeUpdateEvent.getSymbol(), orderTradeUpdateEvent.getExecutionType(), orderTradeUpdateEvent.getPriceOfLastFilledTrade());
                }
            };

            wsCallback.setHandler(userDataUpdates);

            log.info("Listening for UserDataStream events...");
        }
    }

    public void loadUserAccount() {
        // get user account balances
        account = restClient.getAccount();
        // log.info("Account: {}", account);

        if (!account.isCanTrade()) {
            log.info("This account is not allowed to trade. Check your Account KYC.");
            shutdownManager.initiateShutdown(1);
        }

        for (AssetBalance assetBalance : account.getBalances()) {
            balances.put(assetBalance.getAsset(), assetBalance);
        }

        StringBuilder balancesStr = new StringBuilder(StringUtils.EMPTY);
        for (String asset : balances.keySet()) {
            balancesStr.append("\n    ").append(asset).append(": ").append(balances.get(asset).getFree());
        }
        log.info("\nAccount balances: {}", balancesStr);

        userAssets = restClient.getAllAssets();
        // log.info("Account available assets: {}", userAssets);
    }

    public void reconnect() {
        try {
            Thread.sleep(10000);
            connectUserDataStream();
        } catch (Exception ex) {
            log.error("Error: ", ex);
            reconnect();
        }
    }

    @Scheduled(initialDelayString = "600000", fixedRateString = "600000")
    public void printAccountBalances() {
        StringBuilder balancesStr = new StringBuilder(StringUtils.EMPTY);
        for (String asset : balances.keySet()) {
            balancesStr.append("\n    ").append(asset).append(": ").append(balances.get(asset).getFree());
        }
        log.info("\nAccount balances: {}", balancesStr);
    }


    @Scheduled(initialDelayString = "600000", fixedRateString = "600000")
    public void renewListenKey() {
        // We can keep alive the user data stream via a scheduled keepalive
        if (listenKey != null && !listenKey.isBlank()) {
            try {
                log.info("Renewing listenKey: {}", listenKey);
                restClient.keepAliveUserDataStream(listenKey);
            } catch (Exception ex) {
                log.error("Error while renewing listenKey: ", ex);
            }
        }
    }

    public boolean assetIsTradable(String asset) {
        UserAsset userAsset = userAssets.stream().filter(ua -> ua.getCoin().equals(asset)).findFirst().orElse(null);
        return userAsset != null && userAsset.isTrading();
    }

    @Override
    public void onFailure(Throwable cause) {
        reconnect();
    }

    @PreDestroy
    public void close() throws IOException {
        // Or we can invalidate it, whenever it is no longer needed
        if (listenKey != null && !listenKey.isBlank()) {
            try {
                log.info("Closing listenKey: {}", listenKey);
                restClient.closeUserDataStream(listenKey);
            } catch (Exception ex) {
                log.error("Error while renewing listenKey: ", ex);
            }
        }

        if (webSocket != null) {
            webSocket.close();
        }
    }

}
