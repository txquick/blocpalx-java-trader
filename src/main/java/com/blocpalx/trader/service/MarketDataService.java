package com.blocpalx.trader.service;

import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.domain.general.ExchangeInfo;
import com.blocpalx.api.client.domain.general.SymbolInfo;
import com.blocpalx.api.client.domain.general.SymbolStatus;
import com.blocpalx.api.client.domain.market.TickerPrice;
import com.blocpalx.trader.domain.MarketAssetValue;
import com.blocpalx.trader.domain.MarketTradePair;
import com.blocpalx.trader.event.TraderEventPublisher;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Data
@Slf4j
public class MarketDataService {

    private static final List<String> quoteAssets = Arrays.asList("USD", "CAD", "BTC", "ETH", "USDC", "USDT");

    private final BlocPalXApiRestClient restClient;
    private final TraderEventPublisher traderEventPublisher;

    private List<MarketTradePair> marketTradePairs;
    private ConcurrentHashMap<String, MarketAssetValue> marketAssetValues = new ConcurrentHashMap<>();

    @Autowired
    public MarketDataService(BlocPalXApiRestClient restClient,
                             TraderEventPublisher traderEventPublisher) {
        this.restClient = restClient;
        this.traderEventPublisher = traderEventPublisher;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initialize() {
        loadData();
        // log.info("Trade Pairs: {}", marketTradePairs);
        traderEventPublisher.publishMarketDataReadyEvent();
    }

    public void loadData() {
        // get exchangeInfo
        ExchangeInfo exchangeInfo = restClient.getExchangeInfo();
        // log.info("Exchange Info: {}", exchangeInfo);

        marketTradePairs = buildTradePairs(exchangeInfo.getSymbols());

        buildUsdAssetValues();
        // log.info("Asset Values: {}", marketAssetValues);

        // List<BookTicker> bookTickerList = restClient.getBookTickers();
        // log.info("Book Tickers: {}", bookTickerList);
    }

    @Scheduled(initialDelayString = "60000", fixedRateString = "60000")
    public void renew() {
        try {
            loadData();
        } catch (Exception ex) {
            log.error("Error while renewing listenKey: ", ex);
        }
    }

    private void buildUsdAssetValues() {
        // first pass - actual USD
        marketAssetValues.put("USD", new MarketAssetValue("USD", BigDecimal.ONE));

        // second pass - pseudo USD tokens
        for (MarketTradePair marketTradePair : marketTradePairs) {
            if (marketTradePair.getStatus() == SymbolStatus.TRADING) {
                if (marketTradePair.getQuoteAsset().equals("USD")
                        && marketTradePair.getBaseAsset().contains("USD")
                        && marketTradePair.getLastPrice().compareTo(BigDecimal.ZERO) > 0) {
                    marketAssetValues.put(marketTradePair.getBaseAsset(), new MarketAssetValue(marketTradePair.getBaseAsset(), marketTradePair.getLastPrice()));
                } else if (marketTradePair.getQuoteAsset().contains("USD")) {
                    if (marketAssetValues.get(marketTradePair.getQuoteAsset()) == null) {
                        marketAssetValues.put(marketTradePair.getQuoteAsset(), new MarketAssetValue(marketTradePair.getQuoteAsset(), BigDecimal.ONE));
                    }
                }
            }
        }

        // third pass - non-USD quote assets
        for (String quoteAsset : quoteAssets) {
            if (!quoteAsset.contains("USD")) {
                for (MarketTradePair marketTradePair : marketTradePairs) {
                    if (marketTradePair.getStatus() == SymbolStatus.TRADING
                            && marketTradePair.getBaseAsset().equals(quoteAsset)
                            && marketTradePair.getQuoteAsset().contains("USD")
                            && marketTradePair.getLastPrice().compareTo(BigDecimal.ZERO) > 0
                            && marketAssetValues.get(quoteAsset) == null) {
                            marketAssetValues.put(quoteAsset, new MarketAssetValue(quoteAsset, marketTradePair.getLastPrice()));
                    }
                }
            }
        }

        // fourth pass - non-USD base assets
        for (MarketTradePair marketTradePair : marketTradePairs) {
            if (marketTradePair.getStatus() == SymbolStatus.TRADING) {
                if (marketAssetValues.get(marketTradePair.getBaseAsset()) == null) {
                    if (marketTradePair.getQuoteAsset().contains("USD")) {
                        marketAssetValues.put(marketTradePair.getBaseAsset(), new MarketAssetValue(marketTradePair.getBaseAsset(), marketTradePair.getLastPrice()));
                    } else {
                        // eg XMRBTC
                        MarketAssetValue quoteAssetValue = marketAssetValues.get(marketTradePair.getQuoteAsset());
                        if (quoteAssetValue != null) {
                            BigDecimal quoteValueUsd = quoteAssetValue.getValueUsd();
                            BigDecimal baseValueUsd = quoteValueUsd.multiply(marketTradePair.getLastPrice());
                            marketAssetValues.put(marketTradePair.getBaseAsset(), new MarketAssetValue(marketTradePair.getBaseAsset(), baseValueUsd));
                        }
                    }
                }
            }
        }
    }

    private List<MarketTradePair> buildTradePairs(List<SymbolInfo> symbolInfoList) {
        List<TickerPrice> tickerPriceList = restClient.getAllPrices();
        // log.info("Ticker Prices: {}", tickerPriceList);

        List<MarketTradePair> marketTradePairs = new ArrayList<>();
        for (SymbolInfo symbolInfo : symbolInfoList) {
            TickerPrice tickerPrice = tickerPriceList.stream().filter(tp -> tp.getSymbol().equals(symbolInfo.getSymbol())).findFirst().orElse(null);

            MarketTradePair marketTradePair = new MarketTradePair();
            marketTradePair.setSymbol(symbolInfo.getSymbol());
            marketTradePair.setBaseAsset(symbolInfo.getBaseAsset());
            marketTradePair.setQuoteAsset(symbolInfo.getQuoteAsset());
            marketTradePair.setStatus(symbolInfo.getStatus());
            marketTradePair.setLastPrice(tickerPrice != null ? new BigDecimal(tickerPrice.getPrice()) : BigDecimal.ZERO);
            marketTradePairs.add(marketTradePair);
        }
        return marketTradePairs;
    }

    private MarketTradePair extractTradePairFromSymbol(String symbol) {
        MarketTradePair marketTradePair = new MarketTradePair();
        marketTradePair.setSymbol(symbol);
        for (String quoteAsset : quoteAssets) {
            if (symbol.endsWith(quoteAsset)) {
                marketTradePair.setQuoteAsset(quoteAsset);
                marketTradePair.setBaseAsset(symbol.replaceAll(quoteAsset + "$", ""));
            }
        }
        return marketTradePair;
    }
}
