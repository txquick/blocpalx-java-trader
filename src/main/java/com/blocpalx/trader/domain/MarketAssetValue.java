package com.blocpalx.trader.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class MarketAssetValue {

    private String asset;
    private BigDecimal valueUsd;

}
