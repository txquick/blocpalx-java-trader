package com.blocpalx.trader.domain;

import com.blocpalx.api.client.domain.general.SymbolStatus;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class MarketTradePair {

    private String symbol;
    private String baseAsset;
    private String quoteAsset;
    private SymbolStatus status;
    private BigDecimal lastPrice;

}
