package com.blocpalx.trader.factory;

import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.trader.context.TradingContext;
import com.blocpalx.trader.context.TradingVolumeContext;
import com.blocpalx.trader.domain.MarketTradePair;
import com.blocpalx.trader.domain.TradeStrategy;
import com.blocpalx.trader.service.MarketDataService;
import com.blocpalx.trader.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradingContextFactory {

    private final BlocPalXApiRestClient restClient;
    private final MarketDataService marketDataService;
    private final UserDataService userDataService;

    @Autowired
    public TradingContextFactory(BlocPalXApiRestClient restClient,
                                 MarketDataService marketDataService,
                                 UserDataService userDataService) {
        this.restClient = restClient;
        this.marketDataService = marketDataService;
        this.userDataService = userDataService;
    }

    public TradingVolumeContext createNewVolumeTrader(MarketTradePair marketTradePair) {
        return new TradingVolumeContext(restClient, marketDataService, userDataService, TradeStrategy.VOLUME, marketTradePair);
    }

}
