# BlocPalX Java Trader

A simple project template to get you up and trading using the BlocPalX API quickly. 
This project uses the [BlocPalX Java API Client](https://gitlab.com/txquick/blocpalx-api-java-client)
and provides the basic structures for running a trading bot, including:

- Automated websocket connection and reconnection as needed
- Basic Market Data structures, automatically updated at regular intervals
- Basic User Data structures, automatically updated at regular intervals
- Basic Trade Context and Trade Context Factory implementation
- Basic trade strategy template implementation running at 60s intervals

## Getting started

Make sure you have maven and minimum JDK 11

Clone the project and open in your favorite Java IDE to check it all out

Setup an account with BlocPalX

- Sandbox: https://sandbox.blocpal.com
- Production: https://x.blocpal.com

Copy ./src/main/resources/trader.properties.example to ./src/main/resources/trader.properties

Set your environment and add your API keys to ./src/main/resources/trader.properties

Build and run using the manager.sh shell script

## Installation

Coming soon...

## Usage

Coming soon...

## Authors and acknowledgment

Written by Ethan Burnside <burnside@blocpal.com>

## License

(C)opyright 2023 BlocPal International

MIT License - see LICENSE file

