#!/bin/bash

# general settings
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
LOG_DIR=${SCRIPT_DIR}/log/

# services listed must have spaces both sides
SERVICES=" blocpalx-trader "

ENV_PROPERTIES=
NEWRELIC_ENV=development

trader_memory_override="-Xmx256m"


status() {
	local status="[stopped]"
	local PID=0
	if [ -f "/tmp/java-${SERVICE}.pid" ]; then
		PID=$(cat /tmp/java-${SERVICE}.pid)

		if [ $PID -gt 0 ] && ps -p $PID > /dev/null
		then
			status="[running]"

			printf "%-16s \e[1;32m%-12s\e[0m\n" $SERVICE $status
		else
			status="[crashed]"
			printf "%-16s \e[1;31m%-10s\e[0m\n" $SERVICE $status
		fi
	else
		printf "%-16s \e[1;31m%-10s\e[0m\n" $SERVICE $status
	fi
}

start() {
	checkLogDir

	echo "Starting ${SERVICE}..."

	local PID=0
	if [ -f "/tmp/java-${SERVICE}.pid" ]; then
		PID=$(cat /tmp/java-${SERVICE}.pid)
	fi

	if [ $PID -gt 0 ] && ps -p $PID > /dev/null
	then
		echo "	... service ${SERVICE} with $PID is already running"
	else
		# local extra_args="-javaagent:/opt/env_conf/newrelic/newrelic.jar -Dnewrelic.config.app_name=TxQ-Platform-${NEWRELIC_ENV}-${SERVICE} -Dnewrelic.environment=${NEWRELIC_ENV}"
		local extra_args=""
		local port=""

		local memory="-Xmx128m"
		local tmp="$(echo $SERVICE | tr '-' '_')_memory_override"
		if [ ! -z ${!tmp} ]; then
			memory=${!tmp}
		fi

		local JAR_PATH="./target/"
		local JAR_FILE="${SERVICE}.jar"

		cd ${JAR_PATH}
		if [ -f "$JAR_FILE" ]; then
			nohup java ${memory} ${extra_args} -jar ${JAR_FILE} >> "${LOG_DIR}${SERVICE}.log" 2> "${LOG_DIR}${SERVICE}-errors.log" < /dev/null & PID=$!; echo $PID > "/tmp/java-${SERVICE}.pid"
			echo -e "	\e[1;32m... service ${SERVICE} started with PID $(cat /tmp/java-${SERVICE}.pid)\e[0m"
		else
			echo -e "	\e[1;31m... service ${SERVICE} not found\e[0m"
		fi
		cd ../../
	fi
}

stop() {
	echo "Stopping ${SERVICE}..."
	if [ -f "/tmp/java-${SERVICE}.pid" ]; then
		local PID=$(cat /tmp/java-${SERVICE}.pid)
		kill -15 $PID
		echo -e "	\e[1;32m... sent kill to PID $PID\e[0m"
		# some of the daemons need some time to shut down and close threads
		sleep 10
	else
		echo -e "	\e[1;31m... no PID file found\e[0m"
	fi
}

forceStop() {
	if [ -f "/tmp/java-${SERVICE}.pid" ]; then
		local PID=$(cat /tmp/java-${SERVICE}.pid)

		if [ $PID -gt 0 ] && ps -p $PID > /dev/null
		then
			echo
			echo -e "	\e[1;31m... service ${SERVICE} with $PID still running, sending kill -9\e[0m"
			kill -9 $PID
			sleep 2
		fi

		if [ $PID -gt 0 ]
		then
			rm /tmp/java-${SERVICE}.pid
		fi
	fi
}

checkLogDir() {
	if [ ! -w "$LOG_DIR" ]; then
		echo
		echo "$LOG_DIR is not writable"
		echo
		exit 1
	fi
}

checkService() {
	if [[ $SERVICES =~ " ${SERVICE} " ]]; then
		# good
		ABC=123
	else
		echo
		echo -e "\e[1;31mERROR: Invalid service selected\e[0m"
		echo
		usage
		exit
	fi
}

trim() {
	local var="$*"
	# remove leading whitespace characters
	var="${var#"${var%%[![:space:]]*}"}"
	# remove trailing whitespace characters
	var="${var%"${var##*[![:space:]]}"}"
	printf '%s' "$var"
}

usage() {
	echo >&2
	echo "service list:" >&2
	for service in $SERVICES; do
		echo "  $service" >&2
	done
	echo >&2
	echo "library list:" >&2
	for service in $LIBS; do
		echo "  $service" >&2
	done
	echo >&2
	echo "usage:" >&2
	echo "  $0 build <service|library>" >&2
	echo "  $0 build-docker <service|library>" >&2
	echo "  $0 start|stop|restart <service>" >&2
	echo "  $0 generateAesSecret|listOrderBook|status|wipe-logs|build-all|build-docker-all|start-all|stop-all|restart-all" >&2
	echo "" >&2
	echo "maven hints:" >&2
	echo "  set new version: mvn versions:set -DnewVersion=1.0.15-SNAPSHOT" >&2
	echo >&2
	exit 1
}

cd "${0%/*}"

case "$1" in
	wipe-logs)
		cd ${LOG_DIR};
		for file in *; do cat /dev/null >  $file; done
		;;
	status)
		for service in $SERVICES; do
			SERVICE=$service
			status
		done
		;;
	build-all)
		mvn clean
		mvn -N install -DskipTests=true -T 1C
		mvn package -DskipTests=true -T 1C
		;;
	build-docker-all)
		mvn clean
		mvn -N install -DskipTests=true -T 1C
		mvn package docker:build -DskipTests=true
		;;
	start-all)
		for service in $SERVICES; do
			SERVICE=$service
			start
			sleep 15
		done
		;;
	stop-all)
		for service in $SERVICES; do
			SERVICE=$service
			stop
		done
		for service in $SERVICES; do
			SERVICE=$service
			forceStop
		done
		;;
	restart-all)
		for service in $SERVICES; do
			SERVICE=$service
			stop
			sleep 2
			forceStop
			sleep 2
			start
		done
		;;
	build)
		if [ ! -z $2 ]; then
			SERVICE=$2
			if [[ "${LIBS}" == *"${SERVICE}"* ]]; then
				mvn -T 1C -pl ${SERVICE} clean install
				echo >&2
				echo -e "\e[1;31m########################################################\e[0m" >&2
				echo -e "\e[1;31m after rebuilding a library you may need to restart-all\e[0m" >&2
				echo -e "\e[1;31m########################################################\e[0m" >&2
				echo >&2
			else
				mvn -pl ${SERVICE} clean package -DskipTests=true -T 1C -am
			fi
		else
			usage
		fi
		;;
	build-docker)
		if [ ! -z $2 ]; then
			SERVICE=$2
			# if [[ "${SERVICE}" != @(google-auth|common|parity) ]]; then
			if [[ "${LIBS}" == *"${SERVICE}"* ]]; then
				mvn -T 1C -pl ${SERVICE} clean install
				echo >&2
				echo -e "\e[1;31m########################################################\e[0m" >&2
				echo -e "\e[1;31m after rebuilding a library you may need to restart-all\e[0m" >&2
				echo -e "\e[1;31m########################################################\e[0m" >&2
				echo >&2
			else
				mvn -pl ${SERVICE} clean package docker:build -DskipTests=true -am
			fi
		else
			usage
		fi
		;;
	start)
		if [ ! -z $2 ]; then
			SERVICE=$2
			checkService
			start
		else
			usage
		fi
		;;
	stop)
		if [ ! -z $2 ]; then
			SERVICE=$2
			checkService
			stop
			sleep 5
			forceStop
		else
			usage
		fi
		;;
	restart)
		if [ ! -z $2 ]; then
			SERVICE=$2
			checkService
			stop
			sleep 2
			forceStop
			sleep 2
			start
		else
			usage
		fi
		;;
	*)
		usage
		;;
esac
